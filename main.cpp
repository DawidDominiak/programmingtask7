#include <iostream> //Wczytanie zależności
#include <cstdlib>
#include <iomanip>
#include <ctime>
#include <climits>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

struct pair_number_letter { //Definicja struktury range
	char uppercase_letter;
	int number; 
};

/*
* Główna funkcja inicjalizująca program
*/

int main() {
	srand(time(NULL)); //wywołanie srand
	const int n = 10;
	pair_number_letter A[n][n];
	pair_number_letter temp;
	int number_of_odds[n];
	int diagonal_max = INT_MIN;
	int index_x = n - 1;
	int index_y = 0;
	int random_y = rand()%n;


	//Wypełnienie tablicy A[n][n] losowymi liczbami
	for(int i = 0; i < n*n; i++) {
		A[(i/10)][(i%10)].uppercase_letter = (rand()%26) + 65;
		A[(i/10)][(i%10)].number = (rand()%21);
	}
	//Wypełnienie sum[n] zerami
	for(int i = 0; i < n; i++) {
		number_of_odds[i] = 0;
	}

	//Wydrukowanie tablicy wierszami i zliczenie sumy pól liczbowych
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << setw(5) << A[i][j].number << setw(1) << A[i][j].uppercase_letter;
			if(A[i][j].number%2 == 1) {
				number_of_odds[i]++;
			}
		}
		cout << endl;
	}

	//Zastąpienie pól znakowych znakiem @ jeśli suma pól liczbowych jest parzysta
	for(int i = 0; i < n; i++) {
		if(number_of_odds[i]%2 == 0) {
			for(int j = 0; j < n; j++) {
				A[i][j].uppercase_letter = '@';
			}
		}
	}

	cout << endl << endl;

	//Wydrukowanie tablicy wierszami
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << setw(5) << A[i][j].number << setw(1) << A[i][j].uppercase_letter;
		}
		cout << endl;
	}

	//Znalezienie maksymalnej wartości na drugiej przekątnej oraz indeksu tej wartości
	for(int i = 0; i < n; i++) {
		if(A[n-i-1][i].number > diagonal_max) {
			diagonal_max = A[n-i-1][i].number;
			index_x = n-i-1;
			index_y = i;
		}
	}
	cout << endl << endl << "Losujemy wartosc z kolumny: " << random_y + 1 << endl << endl;

	//Zamiana miejscami rekordu z największą wartością pola liczbowego na drugiej przekątnej z ostatniem rekorder z wylosowanej kolumny
	temp = A[n-1][random_y];
	A[n-1][random_y] = A[index_x][index_y];
	A[index_x][index_y] = temp;

	cout << endl << endl;

	//Wydrukowanie tablicy wierszami
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << setw(5) << A[i][j].number << setw(1) << A[i][j].uppercase_letter;
		}
		cout << endl;
	}

	return 0;
}